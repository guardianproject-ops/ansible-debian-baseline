# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [1.0.1](https://gitlab.com/guardianproject-ops/ansible-debian-baseline/compare/0.1.0...1.0.1) (2020-09-01)

* Support Ansible 2.9 and 2.10
